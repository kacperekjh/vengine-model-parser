import fs from 'fs/promises';
import { randomBytes } from 'crypto';

type V3 = { x: number, y: number, z: number };

type Args = {
    inputFile: string,
    outputFile?: string,
    name?: string
};

function parseArgs(): Args {
    if (process.argv.length < 3) {
        console.log('Usage: node index.js input_file [-o=output_file] [-n=const_name]');
        process.exit(1);
    }
    const args: Args = {
        inputFile: process.argv[2]
    };
    const options: Record<string, RegExp> = {
        'outputFile': /-o=(.+)/,
        'name': /-n=(.+)/
    };
    for (let i = 3; i < process.argv.length; i++) {
        for (const key in options) {
            const match = process.argv[i].match(options[key]);
            if (match) {
                (args as any)[key] = match[1];
            }
        }
    }
    return args;
}

const args = parseArgs();

function parseVector(vector: string): V3 {
    const match = vector.match(/(-?[0-9]+.[0-9]+) (-?[0-9]+.[0-9]+) (-?[0-9]+.[0-9]+)/);
    if (!match || match.length < 4) {
        throw new Error(`Failed to parse vector '${vector}'.`);
    }
    return { x: parseFloat(match[1]), y: parseFloat(match[2]), z: parseFloat(match[3]) };
}

function parseVertices(text: string): V3[] {
    const verticesMatch = text.matchAll(/v (.+)\n/gm);
    const vertices = new Array<V3>();
    for (const vertice of verticesMatch) {
        vertices.push(parseVector(vertice[1]));
    }
    return vertices;
}

function faceToPoints(face: string): number[] {
    const match = face.matchAll(/(?: |^)([0-9]+)/g);
    const points = new Array<number>();
    for (const point of match) {
        points.push(parseInt(point[1]));
    }
    if (points.length < 2) {
        throw new Error(`Failed to parse face '${face}'.`);
    }
    points.push(points[0]);
    return points;
}

function parseFaces(text: string): number[][] {
    const facesMatch = text.matchAll(/f (.+)\n/gm);
    const lines = new Array<number[]>();
    for (const face of facesMatch) {
        lines.push(faceToPoints(face[1]));
    }
    return lines;
}

function parseLines(text: string): number[][] {
    const lineMatch = text.matchAll(/l ([0-9]+) ([0-9]+)\n/gm);
    const lines = new Array<number[]>();
    for (const line of lineMatch) {
        lines.push([parseInt(line[1]), parseInt(line[2])]);
    }
    return lines;
}

function optimizeLines(lines: number[][]): number[][] {
    const optimized = new Array<number[]>();
    let modified = false;
    for (const line of lines) {
        // last - first
        let unfinished = optimized.find(v => v.at(-1) === line.at(0));
        if (unfinished) {
            unfinished.push(...line.slice(1));
            modified = true;
            continue;
        }
        // first - last
        unfinished = optimized.find(v => v.at(0) === line.at(-1));
        if (unfinished) {
            unfinished.unshift(...line.slice(0, -1));
            modified = true;
            continue;
        }
        // first - first
        unfinished = optimized.find(v => v.at(0) == line.at(0));
        if (unfinished) {
            unfinished.unshift(...line.reverse().slice(0, -1));
            modified = true;
            continue;
        }
        // last - last
        unfinished = optimized.find(v => v.at(-1) == line.at(-1));
        if (unfinished) {
            unfinished.push(...line.reverse().slice(1));
            modified = true;
            continue;
        }
        // no optimization
        optimized.push(line);
    }
    if (modified) {
        return optimizeLines(optimized);
    }
    else {
        return optimized;
    }
}

function combineVerticesAndLines(vertices: V3[], lines: number[][]): V3[][] {
    const combined = new Array<V3[]>();
    for (const line of lines) {
        const combinedLine = new Array<V3>();
        for (const index of line) {
            if (!(index - 1 in vertices)) {
                throw new Error(`Missing vertex ${index}`);
            }
            combinedLine.push(vertices[index - 1]);
        }
        combined.push(combinedLine);
    }
    return combined;
}

async function writeCode(lines: V3[][]): Promise<void> {
    const uniqueID = randomBytes(8).toString('hex').toUpperCase();
    const name = args.name ? args.name : args.inputFile.replace(/.+\/(.+)(?:\..*)/, '$1');
    let code = 
`#ifndef _${name.toUpperCase()}_${uniqueID}_
#define _${name.toUpperCase()}_${uniqueID}_

#include <vengine>

const std::vector<Line3D> ${name} = {\n`;
    let first = true;
    for (const line of lines) {
        if (!first) {
            code += ',\n';
        }
        first = false;
        code += '\tstd::vector<Vector3>{\n';
        let first2 = true;
        for (const point of line) {
            if (!first2) {
                code += ',\n';
            }
            first2 = false;
            code += `\t\t{${point.x}, ${point.y}, ${point.z}}`;
        }
        code += '}';
    }
    code += '};\n\n#endif';

    await fs.writeFile(args.outputFile ? args.outputFile : args.inputFile + '.h', code);
}

async function main(): Promise<void> {
    let data = '';
    try {
        data = (await fs.readFile(args.inputFile)).toString();
    } catch (e) {
        console.log(`File '${args.inputFile}' doesn't exist or is unreadable.`);
        process.exit(1);
    }
    let result: V3[][];
    try {
        const vertices = parseVertices(data);
        let lines = parseFaces(data);
        lines.push(...parseLines(data));
        lines = optimizeLines(lines);
        result = combineVerticesAndLines(vertices, lines);
    } catch (e) {
        console.log((e as Error).message);
        process.exit(1);
    }
    await writeCode(result);
    process.exit(0);
} 

main();