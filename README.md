# VEngine Model Parser
A small script for converting Wavefront .obj files into VEngine source code written in TypeScript.
## Usage
`node public/index.js input_file [-o=output_file] [-n=const_name]`